<?php

class DriverController
{
    public function postConfirm()
    {
        $error = 0;
        $serviceId = Input::get('service_id');
        $driverId = Input::get('driver_id');

        $service = Service::find($serviceId);

        try {
            DriverValidate::validate($service);
            DriverService::update($service->getId(), $driverId);
            Notifier::notify($service->getUser()->getType(), $service->getUser()->getUUID(), $service->getId());
        } catch (Exception $e) {
            $error = $e->getMessage();
        }

        return Response::json(compact("error"));
    }
}

class DriverService
{
    public static function update(int $serviceId, int $driverId): void
    {
        $driver = Driver::find($driverId);

        Service::update($serviceId, array(
            'driver_id' => $driver->getId(),
            'status_id' => '2',
            'car_id' => $driver->getCarId()
        ));

        Driver::update($driverId, array(
            "available" => '0'
        ));
    }
}

class DriverValidate
{
    public static function validate(Service $service): void
    {
        if ($service == null) {
            throw new Exception(ExceptionConstants::SERVICE_NULL);
        }

        if ($service->getStatusId() == '6') {
            throw new Exception(ExceptionConstants::SERVICE_STATUS_ID_INCORRECT);
        }

        if (!$service->getUser())
            throw new Exception(ExceptionConstants::SERVICE_USER_NOT_EXIST);

        if ($service->getUser()->getUUID() == '') {
            throw new Exception(ExceptionConstants::SERVICE_USER_CORRECT);
        }

        if (!($service->getDriverId() == NULL && $service->getStatusId() == '1')) {
            throw new Exception(ExceptionConstants::DRIVER_ID_NOT_NULL_AND_SERVICE_STATUS_ID_INCORRECT);
        }
    }
}

class Notifier
{
    public static function notify(string $type, string $uuid, int $serviceId): void
    {
        $push = Push::make();

        switch ($type) {
            case Constasts::USER_TYPE_IOS:
                $push->ios($uuid, Constasts::USER_MESSAGE_CONFIRM, 1, 'honk.wav', 'Open', compact('serviceId'));
                break;
            default:
                $push->android2($uuid, Constasts::USER_MESSAGE_CONFIRM, 1, 'default', 'Open', compact('serviceId'));
                break;
        }
    }
}

class Constasts
{
    const USER_TYPE_IOS = '1';
    const USER_MESSAGE_CONFIRM = 'Tu servicio ha sido confirmado!';
}

class ExceptionConstants
{
    const SERVICE_NULL = '3';
    const SERVICE_STATUS_ID_INCORRECT = '2';
    const SERVICE_USER_CORRECT = '0';
    const DRIVER_ID_NOT_NULL_AND_SERVICE_STATUS_ID_INCORRECT = '1';
    const SERVICE_USER_NOT_EXIST = '5';
}
