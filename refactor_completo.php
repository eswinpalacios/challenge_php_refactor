<?php

/* 
    Refactor con clases con valores random para probar los diferentes casos
*/ 

class Input
{
    public static function get($val)
    {
        if ($val == "service_id")
            return 10;

        return 20;
    }
}
class User
{
    public $uuid = 1;
    public $type = "2";

    public function getType()
    {
        return $this->type;
    }

    public function getUUID()
    {
        return $this->uuid;
    }
}
class Service
{
    public $id;
    public $driver_id;
    public $status_id;
    public $user;

    public static function find($id)
    {
        $service = new Service();
        $service->id = $id;
        $service->status_id = "1";
        $service->driver_id = null;

        $service->user = new User();

        return $service;
    }
    public static function update($id)
    {
        $service = new Service();
        $service->id = $id;
        $user = new User();
        $user->uuid = "2";
        $service->user = $user;
        $service->status_id = "1";
        $service->driver_id = 30;

        return $service;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getDriverId()
    {
        return $this->driver_id;
    }

    public function getStatusId()
    {
        return $this->status_id;
    }

    public function getId()
    {
        return $this->id;
    }
}

class Driver
{
    private $id;
    private $car_id = 10;

    public function getCarId()
    {
        return $this->car_id;
    }

    public static function update($id)
    {
    }
    public static function find($id)
    {
        $driver = new Driver();
        $driver->id = 1;
        
        return $driver;
    }

    public function getId()
    {
        return $this->id;
    }
}

class Push
{
    public static function make()
    {
        $push = new Push();
        return $push;
    }

    public function ios()
    {
        echo "Send ios ...</br>";
    }

    public function android2()
    {
        echo "Send android2 ...</br>";
    }
}

class Response
{
    public static function json($arr)
    {
        print_r($arr);
    }
}

class DriverController
{
    public function postConfirm()
    {
        $error = 0;
        $serviceId = Input::get('service_id');
        $driverId = Input::get('driver_id');

        $service = Service::find($serviceId);

        try {
            DriverValidate::validate($service);
            DriverService::update($service->getId(), $driverId);
            Notifier::notify($service->getUser()->getType(), $service->getUser()->getUUID(), $service->getId());
        } catch (Exception $e) {
            $error = $e->getMessage();
        }

        return Response::json(compact("error"));
    }
}

class DriverService
{
    public static function update(int $serviceId, int $driverId): void
    {
        $driver = Driver::find($driverId);

        Service::update($serviceId, array(
            'driver_id' => $driver->getId(),
            'status_id' => '2',
            'car_id' => $driver->getCarId()
        ));

        Driver::update($driverId, array(
            "available" => '0'
        ));
    }
}

class DriverValidate
{
    public static function validate(Service $service): void
    {
        if ($service == null) {
            throw new Exception(ExceptionConstants::SERVICE_NULL);
        }

        if ($service->getStatusId() == '6') {
            throw new Exception(ExceptionConstants::SERVICE_STATUS_ID_INCORRECT);
        }

        if (!$service->getUser())
            throw new Exception(ExceptionConstants::SERVICE_USER_NOT_EXIST);

        if ($service->getUser()->getUUID() == '') {
            throw new Exception(ExceptionConstants::SERVICE_USER_CORRECT);
        }

        if (!($service->getDriverId() == NULL && $service->getStatusId() == '1')) {
            throw new Exception(ExceptionConstants::DRIVER_ID_NOT_NULL_AND_SERVICE_STATUS_ID_INCORRECT);
        }
    }
}

class Notifier
{
    public static function notify(string $type, string $uuid, int $serviceId): void
    {
        $push = Push::make();

        switch ($type) {
            case Constasts::USER_TYPE_IOS:
                $push->ios($uuid, Constasts::USER_MESSAGE_CONFIRM, 1, 'honk.wav', 'Open', compact('serviceId'));
                break;
            default:
                $push->android2($uuid, Constasts::USER_MESSAGE_CONFIRM, 1, 'default', 'Open', compact('serviceId'));
                break;
        }
    }
}

class Constasts
{
    const USER_TYPE_IOS = '1';
    const USER_MESSAGE_CONFIRM = 'Tu servicio ha sido confirmado!';
}

class ExceptionConstants
{
    const SERVICE_NULL = '3';
    const SERVICE_STATUS_ID_INCORRECT = '2';
    const SERVICE_USER_CORRECT = '0';
    const DRIVER_ID_NOT_NULL_AND_SERVICE_STATUS_ID_INCORRECT = '1';
    const SERVICE_USER_NOT_EXIST = '5';
}

$controller = new DriverController();
$controller->postConfirm();
